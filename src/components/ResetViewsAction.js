import { Component } from 'react';
import { connect } from 'react-redux';
import { crudUpdateMany } from 'react-admin';

class ResetViewsAction extends Component {
    componentDidMount = () => {
        const {
            resource,
            basePath,
            selectedIds,
            onExit,
            crudUpdateMany,
        } = this.props;

        crudUpdateMany(resource, selectedIds, { views: 0 }, basePath);
        onExit();
    };

    render() {
        return null;
    }
}

export default connect(undefined, { crudUpdateMany })(ResetViewsAction);