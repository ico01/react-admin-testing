import * as React from 'react';
import { Admin, Resource } from 'react-admin';
import jsonServerProvider from 'ra-data-json-server';


import { PostList, CommentCreate, PostCreate, ProfileIcon, PostEdit } from './pages/PostsAndComments';
import { httpClient } from './helpers/httpClient';
import addUploadFeature from './helpers/addUploadFeature';


export const BasePath = "https://jsonplaceholder.typicode.com"

const App = () => {
    return <Admin dataProvider={addUploadFeature(jsonServerProvider(BasePath, httpClient))} >
        <Resource name="posts" create={PostCreate} edit={PostEdit} icon={ProfileIcon} list={PostList} />
        <Resource name="comments" create={CommentCreate} />
    </Admin >
}

export default App;