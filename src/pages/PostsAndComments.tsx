import * as React from 'react';
import { NumberInput, NumberField, List, Datagrid, Edit, Create, SimpleForm, TextField, TextInput } from 'react-admin';
import BookIcon from '@material-ui/icons/Book';
export const ProfileIcon = BookIcon;

export const PostList = (props) => (
    <List {...props} >
        <Datagrid>
            <NumberField source="id" />
            <NumberField source="userId" />
            <TextField source="title" />
            <TextField source="body" />
        </Datagrid>
    </List>
);

export const PostEdit = (props) => (
    <Edit {...props}>
        <SimpleForm >

            <NumberInput source="id" />
            <NumberInput source="userId" />
            <TextInput source="title" />
            <TextInput source="body" />
        </SimpleForm>
    </Edit>

);


export const PostCreate = (props) =>
    (
        <Create {...props} >
            <SimpleForm >
                <NumberInput source="id" />
                <NumberInput source="userId" />
                <TextInput source="title" />
                <TextInput source="body" />
            </SimpleForm>
        </Create>
    );

export const CommentCreate = (props) =>
    (
        <Create {...props} >
            <SimpleForm >
                <NumberInput source="id" />
                <NumberInput source="postId" />
                <TextInput source="name" />
                <TextInput source="email" />
                <TextInput source="body" />
            </SimpleForm>
        </Create>
    );
