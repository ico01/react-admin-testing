/**
 * Convert a `File` object returned by the upload input into
 * a base 64 string. That's easier to use on FakeRest, used on
 * the ng-admin example. But that's probably not the most optimized
 * way to do in a production database.
 */
const convertFileToBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file.rawFile);

    reader.onload = () => resolve(reader.result);
    reader.onerror = reject;
});

/**
 * For posts update only, convert uploaded image in base 64 and attach it to
 * the `picture` sent property, with `src` and `title` attributes.
 */
const addUploadCapabilities = requestHandler => (type, resource, params) => {
    console.log("addUploadCapabilities", { type, resource, params })
    console.log("params.data", params.data)
    if (type === 'CREATE' && resource === 'files') {

        if (params.data.file) {
            return convertFileToBase64(params.data.file).then(picture64 => ({
                src: picture64,
                title: `${params.data.file.title}`,
            })).then(transformed => {
                return requestHandler(type, resource, {
                    ...params,
                    data: {
                        recordId: params.data.recordId,
                        userId: params.data.userId,
                        md5: params.data.md5,
                        category: params.data.category,
                        file: transformed,
                    },
                })
            })
        }

    }

    return requestHandler(type, resource, params);
};

export default addUploadCapabilities;