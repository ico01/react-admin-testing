import { AUTH_LOGIN, AUTH_LOGOUT, AUTH_CHECK } from 'react-admin';
import { BasePath } from '../App';

export default (type, params) => {
    if (type === AUTH_LOGIN) {
        const { username, password } = params;
        const request = new Request(BasePath + "/login", {
            method: 'POST',
            body: JSON.stringify({ username, password }),
            headers: new Headers({ 'Content-Type': 'application/json' }),
        })
        console.log('request:', request);
        return fetch(request)
            .then(response => {
                if (response.status < 200 || response.status >= 300) {
                    throw new Error(response.statusText);
                }
                return response.json();
            })
            .then(({ token }) => {
                localStorage.setItem('token', token);
                console.log('stored token for later use:', token)
            }).catch(e => {
                console.log('error wor! ', e.message);
            });
    }

    if (type === AUTH_LOGOUT) {
        localStorage.removeItem('token');
    }

    if (type === AUTH_CHECK && !localStorage.getItem('token')) {
        return Promise.reject();
    }

    return Promise.resolve();
}